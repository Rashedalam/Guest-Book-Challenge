<?php
include_once "../../vendor/autoload.php";

use Rashed\utility\Debug;
use Rashed\utility\Validator;

//prepare query
$query = "SELECT * FROM `GuestBook`";

//connect to database
$dbh = new PDO("mysql:host=localhost;dbname=Crud", "root", "");
$result = $dbh->query($query);


?>


<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../asset/css/style.css">

    <title>Contact Form</title>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="#">Navbar</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="index.php">All Contacts</a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="create.html">Create Contact <span class="sr-only">(current)</span></a>
            </li>
        </ul>

    </div>
</nav>


<section>
    <div class="container">

        <h1 class="text-center">Contact List</h1>

        <table class="table table-dark">
            <thead>
            <tr>
                <th scope="col">Id</th>
                <th scope="col">Full Name</th>
                <th scope="col">Email</th>
                <th scope="col">Subject</th>
                <th scope="col">Action</th>
            </tr>
            </thead>
            <tbody>

            <?php
            foreach ($result as $user) {
                ?>
                <tr>
                    <td scope="col"><?= $user['Id'] ?></td>
                    <td scope="col"><?= $user['First Name']." ".$user['Last Name'] ?></td>
                    <td scope="col"><?= $user['Email'] ?></td>
                    <td scope="col"><?= $user['Subject'] ?></td>
                    <td scope="col"><?= $user['Comment'] ?></td>
                </tr>
                <?php
            }
            ?>
            </tbody>
        </table>
    </div>
</section>


<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
</body>
</html>
