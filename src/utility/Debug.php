<?php
/**
 * Created by PhpStorm.
 * User: Rashed Alam
 * Date: 5/2/2018
 * Time: 9:23 PM
 */

namespace Rashed\utility;


class Debug
{
    public static function d($var){
        echo "<pre>";
        print_r($var);
        echo "</pre>";
    }

    public static function dd($var){
        self::d($var);
        die();
        exit();
    }
}